#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms
from hanse_highschool.urls import *
from hanse_hacking_festival.models import *

class AdminNoticeInsertForm(forms.Form):
    content = forms.CharField(required=True, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'공지입력하세염'}))

class AdminProblemInsertForm(forms.Form):
    problem_number = forms.IntegerField()
    title = forms.CharField(widget=forms.Textarea)
    content = forms.CharField(widget=forms.Textarea)
    problem_key = forms.CharField(widget=forms.Textarea)
    score = forms.IntegerField()
    image = forms.CharField(required=False, widget=forms.Textarea)
    available = forms.BooleanField(widget=forms.CheckboxInput)

class AdminProblemUpdateForm(forms.Form):
    problem_number = forms.IntegerField()
    title = forms.CharField(widget=forms.Textarea)
    content = forms.CharField(widget=forms.Textarea)
    problem_key = forms.CharField(widget=forms.Textarea)
    score = forms.IntegerField()
    image = forms.CharField(required=False, widget=forms.Textarea)
    available = forms.BooleanField(widget=forms.CheckboxInput)

class AdminInsertTimeControl(forms.Form):
    page_name = forms.ChoiceField(required=True, widget=forms.Select(attrs={'class':'browser-default'}))
    year = forms.IntegerField(required=True)
    month = forms.IntegerField(required=True)
    day = forms.IntegerField(required=True)
    hour = forms.IntegerField(required=True)
    minute = forms.IntegerField(required=True)
    second = forms.IntegerField(required=True)

    def __init__(self, *args, **kwargs):
        super(AdminInsertTimeControl, self).__init__(*args, **kwargs)
        self.fields['page_name'].choices = self.get_url_name()

    def get_url_name(self):
        res = []
        for i in urlpatterns:
            try:
                res.append((i.name, i.name))
            except AttributeError:
                pass

        return tuple(res)

class AdminUserBanForm(forms.Form):
    user_list = forms.ChoiceField(required=True, widget=forms.Select(attrs={'class':'browser-default'}))

    def __init__(self, *args, **kwargs):
        super(AdminUserBanForm, self).__init__(*args, **kwargs)
        self.fields['user_list'].choices = self.get_user_list()

    def get_user_list(self):
        all_user = HanseiUser.objects.all()
        res = []
        for i in all_user:
            try:
                res.append((i.id, i.name))
            except AttributeError:
                pass

        return tuple(res)