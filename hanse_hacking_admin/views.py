#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib import messages

from hanse_hacking_festival.models import *
from hanse_hacking_festival.forms import *
from hanse_hacking_admin.models import *
from hanse_hacking_admin.forms import *

import time
import datetime

def admin_dashboard(request):
    return render_to_response('admin.html', {}, context_instance=RequestContext(request))

def admin_insert_notice(request):
    form = AdminNoticeInsertForm(request.POST)

    if form.is_valid():
        content = form.cleaned_data['content']
        Hansei_Notice(content=content).save()
    else:
        form = AdminNoticeInsertForm()

    return render_to_response('admin_notice.html', {'form': form}, context_instance=RequestContext(request))

def admin_manage_ban(request):
    user_ban = HanseUserBanList.objects.all()
    form = AdminUserBanForm(request.POST)

    if form.is_valid():
        user_id = form.cleaned_data['user_list']
        HanseUserBanList(user=HanseiUser.objects.get(pk=user_id)).save()
    else:
        form = AdminUserBanForm()

    return render_to_response('admin_user_ban.html', {'form':form, 'user_ban':user_ban}, context_instance=RequestContext(request))

def admin_manage_time_control(request):
    form = AdminInsertTimeControl(request.POST)

    if form.is_valid():
        page_name = form.cleaned_data['page_name']
        year = form.cleaned_data['year']
        month = form.cleaned_data['month']
        day = form.cleaned_data['day']
        hour = form.cleaned_data['hour']
        minute = form.cleaned_data['minute']
        second = form.cleaned_data['second']
        time_control = datetime.datetime(year, month, day, hour, minute, second)
        epoch_time = int(time.mktime(time_control.timetuple()))

        HanseTimeControl(page_name=page_name, time_control=epoch_time).save()
    else:
        form = AdminInsertTimeControl()

    return render_to_response('admin_time_control.html', {'form': form}, context_instance=RequestContext(request))

def admin_problem_insert(request):
    form = AdminProblemInsertForm(request.POST)

    if form.is_valid():
        problem_number = form.cleaned_data['problem_number']
        title = form.cleaned_data['title']
        content = form.cleaned_data['content']
        problem_key = form.cleaned_data['problem_key']
        score = form.cleaned_data['score']
        image = form.cleaned_data['image']
        available = form.cleaned_data['available']

        try:
            Hansei_Problem(problem_number=problem_number, title=title, content=content, problem_key=problem_key, score=score, image=image, available=available).save()
        except:
            messages.info(request, "중복된 문제번호")
    else:
        form = AdminProblemInsertForm()

    return render_to_response('admin_problem.html', {'form': form}, context_instance=RequestContext(request))