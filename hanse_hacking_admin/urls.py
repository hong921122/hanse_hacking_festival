"""hanse_highschool URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url


urlpatterns = [
    url(r'^$', 'hanse_hacking_admin.views.admin_dashboard'),
    url(r'^notice/', 'hanse_hacking_admin.views.admin_insert_notice', name='notice_insert'),
    url(r'^time/', 'hanse_hacking_admin.views.admin_manage_time_control', name='time_insert'),
    url(r'^problem/', 'hanse_hacking_admin.views.admin_problem_insert', name='problem_insert'),
    url(r'^user_ban/', 'hanse_hacking_admin.views.admin_manage_ban', name='user_ban'),
]
