__author__ = 'HongSeokJun'

from hanse_hacking_admin.models import *
from django.http import HttpResponseRedirect
import time

def time_control(page_name=None, redirect_url='/'):
    def wrap(func):
        def wrapper(*args, **kwargs):
            if HanseTimeControl.objects.filter(page_name=page_name).exists():
                if int(time.time()) > int(HanseTimeControl.objects.filter(page_name=page_name)[0].time_control):
                    return func(*args, **kwargs)
                else:
                    return HttpResponseRedirect(redirect_url)
            else:
                return func(*args, **kwargs)
        return wrapper
    return wrap

def user_ban(func):
    print func
    def wrapper(*args, **kwargs):
        if HanseUserBanList.objects.filter(user_id=args[0].user.id):
            return HttpResponseRedirect('/')
        else:
            return func(*args, **kwargs)
    return wrapper