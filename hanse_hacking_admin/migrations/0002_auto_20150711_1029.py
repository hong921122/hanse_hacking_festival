# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hanse_hacking_admin', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hansetimecontrol',
            name='time_control',
            field=models.IntegerField(),
        ),
    ]
