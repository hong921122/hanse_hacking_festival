from django.db import models
from hanse_hacking_festival.models import *

#deprecated
class HansePageControl(models.Model):
    page_name = models.TextField()

    def __unicode__(self):
        return self.page_name

class HanseTimeControl(models.Model):
    # page = models.ForeignKey(NhfPageControl)
    page_name = models.TextField()
    time_control = models.IntegerField()

    def __unicode__(self):
        return unicode(self.time_control)

class HanseUserBanList(models.Model):
    user = models.ForeignKey(HanseiUser)

    def __unicode__(self):
        return self.user