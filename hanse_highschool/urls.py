"""hanse_highschool URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url

import hanse_hacking_admin

urlpatterns = [
    url(r'^$', 'hanse_hacking_festival.views.index', name='index'),
    url(r'^about/', 'hanse_hacking_festival.views.about', name='about'),
    url(r'^notice/', 'hanse_hacking_festival.views.notice', name='notice'),
    url(r'^problem/', 'hanse_hacking_festival.views.problem_list', name='problem_list'),
    url(r'^problem/(?P<problem_id>.*)', 'hanse_hacking_festival.views.problem_detail', name='problem_detail'),
    url(r'^register/$', 'hanse_hacking_festival.views.register', name='register'),
    url(r'^login/', 'hanse_hacking_festival.views.login', name='login'),
    url(r'^logout/', 'hanse_hacking_festival.views.logout', name='logout'),
    url(r'^admin/', include('hanse_hacking_admin.urls'), name='admin'),
]
