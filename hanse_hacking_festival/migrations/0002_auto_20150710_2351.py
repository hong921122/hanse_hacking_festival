# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hanse_hacking_festival', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='hansei_problem',
            old_name='key',
            new_name='problem_key',
        ),
        migrations.RemoveField(
            model_name='hansei_problem',
            name='relative_score',
        ),
        migrations.AddField(
            model_name='hansei_problem',
            name='available',
            field=models.BooleanField(default=False),
        ),
    ]
