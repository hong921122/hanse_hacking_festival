# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hanse_hacking_festival', '0003_auto_20150711_0008'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hansei_problemsolved',
            name='problem_id',
        ),
        migrations.AddField(
            model_name='hansei_problemsolved',
            name='problem',
            field=models.ForeignKey(to='hanse_hacking_festival.Hansei_Problem', default=-1, to_field=b'problem_number'),
        ),
    ]
