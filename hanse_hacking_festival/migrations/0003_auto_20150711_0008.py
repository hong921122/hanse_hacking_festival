# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hanse_hacking_festival', '0002_auto_20150710_2351'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hansei_problem',
            name='image',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='hansei_problem',
            name='problem_number',
            field=models.IntegerField(unique=True),
        ),
    ]
