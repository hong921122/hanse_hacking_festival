#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms
from django.core.validators import RegexValidator

import re

class HanseiUserForm(forms.Form):
	error_css_class = 'blink'
	required_css_class = 'danger'

	username = forms.CharField(max_length=20, required=True, widget=forms.TextInput(attrs={'placeholder':'아이디를 입력해주세요'}), validators=[RegexValidator(regex=re.compile('^[a-zA-Z0-9가-힣]+$'), message='올바른 값을 입력해주세요.')] )
	password = forms.CharField(max_length=20, widget=forms.PasswordInput(attrs={'placeholder': '비밀번호를 입력해주세요'}))
	password_confirm = forms.CharField(max_length=20, widget=forms.PasswordInput(attrs={'placeholder':'비밀번호확인'}))
	nickname = forms.CharField(max_length=20, widget=forms.TextInput(attrs={'placeholder':'닉네임을 입력해주세요'}), validators=[RegexValidator(regex=re.compile('^[a-zA-Z0-9가-힣]+$'), message='올바른 값을 입력해주세요.')] )
	name = forms.CharField(max_length=10, widget=forms.TextInput(attrs={'placeholder':'이름을 입력해주세요'}), validators=[RegexValidator(regex=re.compile('^[a-zA-Z0-9가-힣]+$'), message='올바른 값을 입력해주세요.')] )
	email = forms.EmailField(max_length=50, widget=forms.EmailInput(attrs={'placeholder':'이메일을 입력해주세요'}))
	phone_number = forms.CharField(max_length=11, widget=forms.TextInput(attrs={'placeholder':'핸드폰 번호를 입력해주세요'}), validators=[RegexValidator(regex=re.compile('^[0-9]+$'), message='올바른 값을 입력해주세요.')] )
	school = forms.CharField(max_length=20, required=False, widget=forms.TextInput(attrs={'placeholder':'학교를 입력해주세요'}), validators=[RegexValidator(regex=re.compile('^[a-zA-Z0-9가-힣]+$'), message='올바른 값을 입력해주세요.')])
	date_of_birth = forms.DateField(widget=forms.DateInput(attrs={'class':'datepicker', 'type':'date'}))


class HanseiUserLoginForm(forms.Form):
	error_css_class = 'blink'
	required_css_class = 'danger'

	username = forms.CharField(max_length=20, required=True, widget=forms.TextInput(attrs={}))
	password = forms.CharField(max_length=20, required=True, widget=forms.PasswordInput(attrs={}))

class AuthKeyForm(forms.Form):
    key = forms.CharField(required=True, widget=forms.TextInput(attrs={}))