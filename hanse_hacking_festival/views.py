#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.http import HttpResponseRedirect

from hanse_hacking_festival.forms import *
from hanse_hacking_festival.models import *
from hanse_hacking_admin.decorators import *

def index(request):
    request.encoding = 'utf-8'
    return render_to_response('index.html', {}, context_instance=RequestContext(request))

def about(request):
    request.encoding = 'utf-8'
    return render_to_response('about.html', {}, context_instance=RequestContext(request))

# @user_ban
# @time_control(page_name='notice', redirect_url='/')
def notice(request):
    request.encoding = 'utf-8'
    notice_list = Hansei_Notice.objects.all().values().order_by('-create_time')
    return render_to_response('notice.html', {'notice_list': notice_list}, context_instance=RequestContext(request))

@user_ban
@time_control(page_name='problem', redirect_url='/')
def problem_list(request):
    request.encoding = 'utf-8'
    print "jjtj2222"


@login_required(login_url='/login/')
@user_ban
@time_control(page_name='problem', redirect_url='/')
def problem_detail(request, problem_id=None):
    request.encoding = 'utf-8'

    if not problem_id:
        problem_total = Hansei_Problem.objects.all().order_by('problem_number')
        problem_total_list = []
        for problem in problem_total:
            if Hansei_ProblemSolved.objects.filter(problem_id=problem.problem_number, user_id=request.user.id).exists():
                problem.solve = True
                problem_total_list.append(problem)
            else:
                problem.solve = False
                problem_total_list.append(problem)

        return render_to_response('problem.html', {'problem_list':problem_total_list}, context_instance=RequestContext(request))
    else:
        #비정상적인 접근
        if not Hansei_Problem.objects.get(problem_number=problem_id).available:
            return HttpResponseRedirect('/problem')

        problem = Hansei_Problem.objects.get(problem_number=problem_id)
        form = AuthKeyForm(request.POST)

        #이미 인증한 사용자
        if Hansei_ProblemSolved.objects.filter(problem_id=problem_id, user_id=request.user.id):
            form.fields['key'].widget.attrs['readonly'] = 'readonly'
            form.fields['key'].widget.attrs['placeholder'] = 'you have solved'
            return render_to_response('problem_detail.html', {'problem': problem}, context_instance=RequestContext(request))

        if form.is_valid():
            key = form.cleaned_data['key']
            #재인증 막기
            if problem.problem_key == key and not Hansei_ProblemSolved.objects.filter(user_id=request.user.id, problem_id=problem_id):
                bonus_score = break_score(problem_id, request.user)
                Hansei_ProblemSolved(user_id=request.user.id, problem_id=problem_id)
                user_score = HanseiUser.objects.get(pk=request.user.id)
                user_score.score = F('score') + problem.score + bonus_score
                user_score.save()
                messages.info(request, '키 인증 성공')

                return HttpResponseRedirect('/problem')
            else:
                messages.info(request, '키 인증 실패')

        return render_to_response('problem_detail.html', {'auth_form': form, 'problem': problem}, context_instance=RequestContext(request))

@user_ban
@time_control(page_name='register', redirect_url='/')
def register(request):
    request.encoding = 'utf-8'

    if request.method == 'POST':
        form = HanseiUserForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirm = form.cleaned_data['password_confirm']
            nickname = form.cleaned_data['nickname']
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            phone_number = form.cleaned_data['phone_number']
            school = form.cleaned_data['school']
            date_of_birth = form.cleaned_data['date_of_birth']
             # and HanseiUser.objects.filter(nickname=nickname)
            if password == password_confirm:
                HanseiUser.objects.create_user(username=username, password=password, nickname=nickname, name=name, email=email, phone_number=phone_number, school=school, date_of_birth=date_of_birth)
                HttpResponseRedirect('/')
            else:
                HttpResponseRedirect('/')
    else:
        form = HanseiUserForm()

    return render_to_response('register.html', {'form': form}, context_instance=RequestContext(request))

@user_ban
@time_control(page_name='login', redirect_url='/')
def login(request):
    request.encoding = 'utf-8'

    if request.method == 'POST':
        form = HanseiUserLoginForm(request.POST)
        if form.is_valid():
            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            if user is not None:
                auth_login(request, user)
                return HttpResponseRedirect('/')
            else:
                return HttpResponseRedirect('')
    else:
        form = HanseiUserLoginForm()

    return render_to_response('login.html', {'form': form}, context_instance=RequestContext(request))

def logout(request):
    request.encoding = 'utf-8'
    auth_logout(request)
    return HttpResponseRedirect('/')

def break_score(problem_number, auth_user):
    if Hansei_Problem.objects.get(problem_number=problem_number).first:
        problem = Hansei_Problem.objects.get(problem_number=problem_number)
        problem.first = auth_user.username
        problem.save()
        return 3
    elif Hansei_Problem.objects.get(problem_number=problem_number).second:
        problem = Hansei_Problem.objects.get(problem_number=problem_number)
        problem.second = auth_user.username
        problem.save()
        return 2
    elif Hansei_Problem.objects.get(problem_number=problem_number).third:
        problem = Hansei_Problem.objects.get(problem_number=problem_number)
        problem.third = auth_user.username
        problem.save()
        return 1
    else:
        return 0
