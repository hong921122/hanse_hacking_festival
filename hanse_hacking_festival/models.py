from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager

class MyUserManager(BaseUserManager):
	def create_user(self, username, email, date_of_birth, nickname, phone_number, school, password, name):
		if not username:
			raise ValueError("user name field")
		user = self.model(username = username, email = email, date_of_birth = date_of_birth, nickname = nickname, phone_number = phone_number, school = school, name = name)
		user.set_password(password)
		user.save(self._db)

		return user
	def create_superuser(self, username, date_of_birth, nickname, phone_number, school, password, name):
		user = self.create_user(username = username, date_of_birth = date_of_birth, nickname = nickname, phone_number = phone_number, school = school, name = name, password=password, email='')
		user.is_superuser = True
		user.is_staff = True
		user.save(using=self._db)
		return user

class HanseiUser(AbstractUser):
	date_of_birth = models.DateTimeField()
	nickname = models.TextField()
	phone_number = models.TextField()
	school = models.TextField()
	name = models.TextField()
	score = models.IntegerField(default=0)

	objects = MyUserManager()

	REQUIRED_FIELDS = ['date_of_birth', 'nickname', 'phone_number', 'school', 'name']

	def __unicode__(self):
		return self.username

	def has_perm(self, perm, obj=None):
		return True

	def has_module_pers(self, app_label):
		return True

class Hansei_Problem(models.Model):
    problem_number = models.IntegerField(unique=True)
    title = models.TextField()
    content = models.TextField()
    problem_key = models.TextField()
    score = models.IntegerField()
    image = models.TextField(null=True)
    available = models.BooleanField(default=False)

    def __unicode__(self):
        return unicode(self.problem_number)

class Hansei_ProblemSolved(models.Model):
	user = models.ForeignKey(HanseiUser)
	problem = models.ForeignKey(Hansei_Problem, to_field='problem_number', default=-1)
	create_time = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return unicode(self.problem_id)

class Hansei_Notice(models.Model):
	content = models.TextField()
	create_time = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.content